import UIKit

//Playground base Swift

//var vs let

//todo es un objeto

//Colecciones: tupla vs Array vs Set vs Dictionary

//tuplas

//Array

//Creamos un array

//var numbers: [Int] = [2,4,6,8,10]  Definición de tipo explícita

var numbers = [2,4,6,8,10]   // Inferencia de tipos,  array como [Int]


//algunas propiedades arrays

numbers.count
numbers.isEmpty

//algunos métodos predefinidos
numbers.max()
numbers.min()
numbers.reverse()
numbers.enumerated()




//añadimos un elemento al array
numbers.append(12)

//ordenamos de menor a mayor
numbers.sort()


//Array vacío
var vacio1: [Int] = []

//otro forma o realmente lo que ocurre cuando ejecutamos algo como lo de arriba

var vacio2 = [Int]()  // Llamada al constructor por defecto
print("someInts is of type [Int] with \(vacio2.count) items.")
// Prints "someInts is of type [Int] with 0 items."

vacio1.append(3)
vacio2.append(5)
vacio1.append(7)



//Swift 4 nos permite montar arrays en modo:

var doubles = Array(repeating: 2.0, count: 5)

//como hemos definido el array con var sería mutable
doubles[3] = 15
doubles


var anotherDoubles = Array(repeating: 3.0, count: 5)

//y funcionará perfectamente la sobrecarga operador + en arrays
var add: [Double] = doubles + anotherDoubles

//Iteramos en arrays

if doubles.isEmpty {
    print("The array is empty.")
} else {
    
    for (index, value) in doubles.enumerated(){ //enumerated va dando a index de valores de 0 a n-1 en array doubles
        print("Elemento \(index+1): \(value)")
    }
    
}


// Strings en Swift

//var emptyString: String = ""        // cadena vacía: declaración explícita
var emptyString = ""               // cadena vacía: inferencia de tipos
var anotherEmptyString = String()  // sintaxis con inicializador
// Ambas cadenas están vacías y son equivalentes

emptyString.count
anotherEmptyString.count
//ambas son objetos, todo son objetos



//En Swift 4 tenemos por fin de nuevo los String como colecciones

var nombre1: String = "Paul"  //declaración explícita
var nombre2 = "John"          //inferencia de tipos


//mostramos caracteres de 'nombre1' separados por espacios

for caracter in nombre1{
    print("\(caracter) ")
}

// mostramos vocales contenidas en 'nombre2'

var vowels = ["a", "e", "i", "o", "u"] // [String] crearemos luego como un Set<String>

print("Mostramos sólo vocales")

for caracter in nombre2{
    if (vowels.contains(String(caracter))){
        print("\(caracter) ")
    }
    
    
}




// mostramos consonantes contenidas en 'nombre2'

print("Mostramos sólo consonantes")

nombre2 = "Pepe"  //Saldrá repetida p en el conjunto si trabajamos con arrays para mejorar este punto trabajaremos más adelante con Set
for caracter in nombre2{
    if (vowels.contains(String(caracter)) == false){
        print("\(caracter) ")
    }
}


// Sets : arrays sin elementos repetidos, al añadir un elemento si ya existe, no se insertará de nuevo

var colores:Set<String> = ["rojo", "verde", "azul"]
colores.insert("amarillo")
colores.insert("rojo")


//Practicamos con Set

var vowelsSet : Set<String> = ["a", "e", "i", "o", "u"]  //<String> los tipos marcados con <  > serán un tipo genérico que más adelante trabajaremos con mayor detalle, de momento tan sólo debemos entender que marcamos tipo de dato de los elementos que va a almacenar el Set
var vowelsInString: Set<String> = []  // Set vacío

//Creamos Set con vocales contenidas en 'nombre2'
nombre2 = "Almudena"

print("Print consonantes de nombre \(nombre2) en Set")
for caracter in nombre2{
    if (vowelsSet.contains(String(caracter))){
        vowelsInString.insert(String(caracter))
    }
    
    
}
vowelsInString


//Paso a minúsculas de un String
var nombre = "Paula"
nombre = nombre.lowercased()


//Creamos Sets con consonantes contenidas en una lista de nombres")

//normalizamos pasando todos a minúscula
var students = ["Juan", "Paco", "Luis", "Sebas", "Ariel"]

for (index,name) in students.enumerated(){
    students[index] = name.lowercased()
}

students

var consonantesInString : Set<String> = []

for name in students{
    for caracter in name{
        if (vowelsSet.contains(String(caracter)) == false){
            consonantesInString.insert(String(caracter))
        }
        
    }
}

consonantesInString  // realmente sería necesario hacer en modo previo un paso a mayúsculas o minúsculas general

consonantesInString.sorted()






//operador + en String

var name = "John"
var surname = "Brew"
var completeName = surname + ", " + name




//Diccionario: estructura de datos perfecta para asociar información


//Dado un diccionario con alumnos y sus notas en formato numérico, obtener diccionario con notas en formato calificación general
// 8Modificar dejando notas en todo el playground con Float)

var students_notes: [String:Double] = ["Juan" : 9.0, "Paco" : 2.9, "Luis": 9.8, "Sebas" : 2.3]


func Califica1(students_notes: [String: Double]) -> [String: String]{
    
    var calificaciones: Dictionary<String, String> = [:]
    
    for (key , value) in students_notes{
        if (value < 5.0)
        {
            calificaciones[key] = "Insuficiente"
        }
        else{
            if (value < 6.0){
                calificaciones[key] = "Suficiente"
            }
            else{
                if (value < 7.0){
                    calificaciones [key] = "Bien"
                }
                else if (value < 8.5){
                    calificaciones [key] = "Notable"
                }
                else{
                    calificaciones[key] = "Sobresaliente"
                }
            }
        }
    }
    
    return calificaciones
}

students_notes

var resultado = Califica1(students_notes: students_notes)
resultado




//Dada una lista de números, saca por pantalla aquéllos que sean primos


var numeros = [5, 4, 2, 11, 9]
numeros.sort()

//Primo
func Primo (num: Int)-> Bool{
    var esPrimo = true
    var divisor = 2
    
    while (esPrimo && num>divisor){
        if (num % divisor == 0){
            esPrimo = false
        }
        else{
            divisor = divisor + 1
        }
    }
    
    return esPrimo
}


//Primos
for num in numeros{
    if (Primo(num:num)) {
        print ("El numero \(num) es primo")
    }
}



//Deberes: función que devuelva la lista de números primos en un array + llamada a la función y visualización de resultado



//Dada una lista de números, sacamos por pantalla factoriales de los mismos

func Factorial(num: Int) -> Int{
    var resultado = 1
    var numero = num
    while (numero > 1){
        resultado = resultado * numero
        numero = numero - 1
    }
    
    return resultado
}

//Factoriales
for num in numeros{
    print ("El factorial de \(num) es \(Factorial(num:num))")
    }










//función que recibe diccionario de alumnos y notas asociadas y devuelve diccionario con tupla (nombre,nota) asociados a estudiante con mayor y menor calificación


func MaxMin (students_notes: [String: Double]) -> [String: (String, Double)]
{
    var max:Double = -1
    var min:Double = 11
    //var student_max: String = ""
    
    var resultado: [String: (String, Double)] = [:]
    
    for (nombre, nota)  in students_notes{
        if nota > max{
            resultado["student_max"] = (nombre, nota)
            max = nota
            //student_max = nombre
        }
        
        if nota < min{
            resultado["student_min"] = (nombre, nota)
            min = nota
            //student_max = nombre
        }
        
        
        
    }
    
    
    return (resultado)
    
}



//Deberes: Mejorar esta llamada y salida

var resul = MaxMin(students_notes: students_notes)

if let mayor = resul["student_max"]{
    
    print( "El alumno \(mayor.0) sacó la máxima puntuación: \(mayor.1)")
}

if let menor = resul["student_min"]{
    
    print( "El alumno \(menor.0) sacó la mínima puntuación: \(menor.1)")
}





//Ejemplos zip, filter y map


